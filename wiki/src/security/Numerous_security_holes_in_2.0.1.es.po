# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-09-18 06:11+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Sun, 06 Mar 2016 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Sun, 06 Mar 2016 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 2.0.1\"]]\n"
msgstr "[[!meta title=\"Varios agujeros de seguridad en Tails 2.0.1\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"Several security holes that affect Tails 2.0.1 are now fixed in Tails 2.2."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** encourage you to [[upgrade to Tails 2.2|news/version_2.2]] "
"as soon as possible."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "Detalles\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Tor Browser: [[!mfsa 2016-16]], [[!mfsa 2016-17]], [[!mfsa 2016-20]],\n"
"               [[!mfsa 2016-21]], [[!mfsa 2016-23]], [[!mfsa 2016-24]],\n"
"               [[!mfsa 2016-25]], [[!mfsa 2016-27]], [[!mfsa 2016-28]],\n"
"               [[!mfsa 2016-31]], [[!mfsa 2016-34]], [[!mfsa 2016-35]],\n"
msgstr ""
"* Tor Browser: [[!mfsa 2016-16]], [[!mfsa 2016-17]], [[!mfsa 2016-20]],\n"
"               [[!mfsa 2016-21]], [[!mfsa 2016-23]], [[!mfsa 2016-24]],\n"
"               [[!mfsa 2016-25]], [[!mfsa 2016-27]], [[!mfsa 2016-28]],\n"
"               [[!mfsa 2016-31]], [[!mfsa 2016-34]], [[!mfsa 2016-35]],\n"

#. type: Plain text
#, no-wrap
msgid "               [[!mfsa 2016-37]]\n"
msgstr ""

#. type: Bullet: '* '
msgid "graphite2: [[!debsa2016 3479]]"
msgstr ""

#. type: Bullet: '* '
msgid "glibc: [[!debsa2016 3481]]"
msgstr ""

#. type: Bullet: '* '
msgid "libreoffice: [[!debsa2016 3482]]"
msgstr ""

#. type: Bullet: '* '
msgid "cpio: [[!debsa2016 3483]]"
msgstr ""

#. type: Bullet: '* '
msgid "libssh2: [[!debsa2016 3487]]"
msgstr ""

#. type: Bullet: '* '
msgid "pillow: [[!debsa2016 3499]]"
msgstr ""

#. type: Bullet: '* '
msgid "openssl: [[!debsa2016 3500]]"
msgstr ""

#. type: Bullet: '* '
msgid "perl: [[!debsa2016 3501]]"
msgstr ""

#. type: Bullet: '* '
msgid "linux: [[!debsa2016 3503]]"
msgstr ""

#. type: Bullet: '* '
msgid "libav: [[!debsa2016 3506]]"
msgstr ""

#. type: Bullet: '* '
msgid "jasper: [[!debsa2016 3508]]"
msgstr ""
